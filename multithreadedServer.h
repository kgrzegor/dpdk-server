//
// Created by kgrzegor on 24.05.19.
//

#ifndef DPDK_SERVER_MULTITHREADEDSERVER_H
#define DPDK_SERVER_MULTITHREADEDSERVER_H


#include "server.h"
#include <sys/sysinfo.h>
#include <thread>
#include <mutex>
#include <sstream>


class MultithreadedServer : public Server {
private:
    const int N_PROC_;
    std::vector <int> socket_fds_;
    std::vector <addrinfo*> sockets_;
    std::vector <std::thread> threads_;
    std::vector <char *> buffers_;
    std::mutex new_client_;
    long next_port_;

    void processHELLO(sockaddr peer_addr, socklen_t peer_addr_len) override;

    void processTELLME(sockaddr peer_addr, socklen_t peer_addr_len, std::string request) override;

    void sendPackets(int packetCount, int packetSize, int intervalBetweenPackets, int timeBeforeDone,
                     sockaddr peer_addr, socklen_t peer_addr_len);

    void receivePackets(addrinfo* socket);

    void initSendingThread();

public:
    MultithreadedServer();

    virtual ~MultithreadedServer();

    void initThreads();

    void startThreads();

    void startThread(int socket_fd, char *buffer);

    /**
     * Send given data until a response from server is read.
     * @param data pointer to char array where response will be saved.
     * @param message char array to be sent
     */
    void sendImportantPacket(char *data, std::string message);
};


#endif //DPDK_SERVER_MULTITHREADEDSERVER_H
