#include <iostream>
#include <boost/program_options.hpp>
#include "server.h"
#include "client.h"
#include "multithreadedServer.h"

namespace po = boost::program_options;


int main(int argc, char *argv[])
{
    size_t packetSize;
    int packetCount; //TODO: Make use of these variables
    int interval[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384};
    int packetLength[] = {64, 128, 256, 512, 1024};
    int intervalBetweenPackets, timeBeforeDone;
    char serverMode;
    std::string clientMode;
    std::string serverAddress;

    po::options_description visibleOptions("Allowed options");
    visibleOptions.add_options()
            ("help,h", "Show this help message")
            ("packet-size,s", po::value<size_t>(&packetSize)->default_value(64), "Set packet size")
            ("packet-count,c", po::value<int>(&packetCount)->default_value(1), "Set packet count sent by client")
            ("interval-between-packets,i", po::value<int>(&intervalBetweenPackets)->default_value(3000),
             "Set time interval between send packets in micro-seconds")
            ("time-before-done,t", po::value<int>(&timeBeforeDone)->default_value(3000000),
             "Set interval between last packet and received packet count packet")
            ("server-threads,n", po::value<char>(&serverMode)->default_value('s'),
             "possible options are: s - for single thread server, m - for multi thread server")
            ("server-address,a", po::value<std::string>(&serverAddress)->default_value("127.0.0.1"),
             "Set server address")
            ("client-mode,m", po::value<std::string>(&clientMode)->default_value("send"),
             "possible modes are: send, send_test, recv, recv_test");

    po::options_description hiddenOptions("Hidden options");
    hiddenOptions.add_options()("execution_mode", po::value<std::string>(), "launch as server or client");

    po::positional_options_description positionalOptions;
    positionalOptions.add("execution_mode", 1);

    po::options_description commandLineOptions;
    commandLineOptions.add(visibleOptions).add(hiddenOptions);

    po::variables_map optionsMap;
    po::store(po::command_line_parser(argc, argv).options(commandLineOptions).positional(positionalOptions).run(),
              optionsMap);
    po::notify(optionsMap);

    if (optionsMap.count("help")) {
        std::cout << "Usage: " << argv[0] << " 'client' | 'server' [options]" << std::endl;
        std::cout << visibleOptions << std::endl;
        return 0;
    }

    if (optionsMap.count("execution_mode") != 1) {
        std::cout << "You have to specify 'client' or 'server' execution mode" << std::endl;
        std::cout << "Usage: " << argv[0] << " 'client' | 'server' [options]" << std::endl;
        std::cout << visibleOptions << std::endl;
        return -1;
    }

    std::string executionMode = optionsMap["execution_mode"].as<std::string>();
    if (executionMode == "server") {
        if (serverMode == 's') {
            Server server;
            std::cout << server.getIP() << ":" << Server::getPort() << "\n";
            for (;;) // i to się nigdy nie skończy! <3
                try {
                    server.receive();
                }
                catch (const std::runtime_error &error) {
                    std::cerr << error.what();
                    sleep(1);
                }
        }
        else if (serverMode == 'm') {
            MultithreadedServer server;
            std::cout << server.getIP() << ":" << Server::getPort() << "\n";

            for (;;) // i to się nigdy nie skończy! <3
                try {
                    server.receive();
                }
                catch (const std::runtime_error &error) {
                    std::cerr << error.what();
                    sleep(1);
                }
        }
    }
    else {
        if (clientMode == "send") {
            Client client(packetSize, serverAddress);
//            std::cout << "Length: " << packetSize << " interval: " << intervalBetweenPackets << std::endl;
            client.startSending(packetCount, intervalBetweenPackets, timeBeforeDone);
        }
        else if (clientMode == "send_test") {
            for (int i = 0; i < (sizeof(interval) / sizeof(int)); i++) {
                for (int j = 0; j < (sizeof(packetLength) / sizeof(int)); j++) {
                    Client client(packetLength[j], serverAddress);
                    std::cout << "Length: " << packetLength[j] << " interval: " << interval[i] << " Server received: ";
                    client.startSending(1000, interval[i], 300000);
                    std::cout << std::endl;
                }
            }
        }
        else if (clientMode == "recv") {
            Client client(packetSize, serverAddress);
//            std::cout << "Packet size: " << packetSize << " interval between packets: "
//                      << intervalBetweenPackets << std::endl;
            client.startReceiving(packetCount, intervalBetweenPackets, timeBeforeDone);
        }
        else if (clientMode == "recv_test") {
            //TODO:
        }
    }

    return 0;
}
