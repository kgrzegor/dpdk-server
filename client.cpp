//
// Created by kgrzegor on 12.04.19.
//

#include "client.h"
#include "server.h"
#include <stdio.h>
#include <stdlib.h>


Client::Client(size_t packet_size, std::string addressIP):packet_size_(packet_size), SERVER_IP_(addressIP)
{
    if (packet_size_< 64)
        throw std::runtime_error("Packet size has to be at least 64 bytes");

    getServerInfo(Server::getPort().c_str());
    initSocketFd();
    initTimeout();
    connectToServer();
}

void Client::startSending(int packetCount, int intervalBetweenPackets, int timeBeforeDone)
{
    char data[packet_size_];

    sendImportantPacket(data, "1 HELLO");

    connectToNewPort(data); // TEST PENDING

    for (int i = 0; i < packetCount; ++i) {
        sprintf(data, "2 PACKET_ID %d", i);
        std::chrono::system_clock::time_point timeLimit = std::chrono::system_clock::now() +
                                                          std::chrono::microseconds(intervalBetweenPackets);
        send(data, packet_size_); //this may be working wrong, it may sends only 20-ish bytes of data
        std::this_thread::sleep_until(timeLimit);
    }

    std::this_thread::sleep_for(std::chrono::microseconds(timeBeforeDone));
    // connectToNewPort(Server::getPort().c_str());

    sendImportantPacket(data, "3 DONE");

    std::cout << data;

    sendImportantPacket(data, "4 CLEAR");
}

void Client::startReceiving(int packetCount, int intervalBetweenPackets, int timeBeforeDone)
{
    char data[packet_size_];
    int receivedPacketCount = 0;

    sendImportantPacket(data, "0," + std::to_string(packetCount) + "," + std::to_string(packet_size_) + "," +
                        std::to_string(intervalBetweenPackets) + "," + std::to_string(timeBeforeDone));

    if (data[0] == '3') {
        std::cout << "Can not request data from single thread server!\n";
        return;
    }

    sockaddr peer_addr = {};
    socklen_t peer_addr_len = sizeof(sockaddr);

    setTimeout(10, 0);

    for (;;) {
        ssize_t bytes_read = recvfrom(socket_fd_, buffer_, BUFFER_SIZE_, flags_, &peer_addr, &peer_addr_len);

        if (bytes_read == -1) {
            std::cerr << "Failed to receive request -- timeout\n";
            break;
        }
//<debug>
        else if (bytes_read < BUFFER_SIZE_)
            buffer_[bytes_read] = '\0';
        else
            buffer_[BUFFER_SIZE_-1] = '\0';
//</debug>
        if (buffer_[0] == '3') {
            break;
        }
        else {
            ++receivedPacketCount;
        }
    }

    std::cout << receivedPacketCount;
}

/**
 * sends an array of chars over a socket. Socket must be initialized earlier.
 * @param data char array to send
 * @param size of a data to send
 */
void Client::send(char* data, int size)
{
    send(static_cast<const char*>(data), size);
}

/**
 * sends an array of chars over a socket. Socket must be initialized earlier.
 * @param data char array to send
 * @param size of a data to send
 */
void Client::send(const char* data, int size)
{
    if (write(socket_fd_, data, size) != size)
        throw std::runtime_error("Can't send data");
}

/**
 * create socket connected to new given port.
 * @param port_number
 */
void Client::connectToNewPort(const char* port_number) {
    getServerInfo(port_number);
    connectToServer();
}

/**
 * Receive bytes from a socket
 * @param data pointer to char array where received data will be saved.
 * @param size size of a data to read.
 */
void Client::receiveFromServer(char *data, size_t size) {
    ssize_t bytes_read = recvfrom(socket_fd_, data, size, 0, nullptr, nullptr);
    if (bytes_read == -1)
        throw std::runtime_error("Failed to receive from server -- ");
}

/**
 * Save server address info in class variable socket_
 * @param port_number
 */
void Client::getServerInfo(const char *port_number) {
    if (getaddrinfo(SERVER_IP_.c_str() , port_number, &Server::HINTS, &socket_) != 0)
        throw std::runtime_error("Can't get server info");
}

/**
 * Initialize socketFd basing on socket_ variable.
 */
void Client::initSocketFd() {
    socket_fd_ = socket(socket_->ai_family, socket_->ai_socktype, socket_->ai_protocol);
    if (socket_fd_ == -1)
        throw std::runtime_error("Could not create socket\n");
}

void Client::setTimeout(time_t seconds, int microSeconds)
{
    timeval read_timeout = {};
    read_timeout.tv_sec = seconds;
    read_timeout.tv_usec = microSeconds;
    setsockopt(socket_fd_, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
}
/**
 * set socket receive timeout to class variable _timeout
 */
void Client::initTimeout() {
    timeval read_timeout = {};
    read_timeout.tv_sec = 0;
    read_timeout.tv_usec = timeout_;
    setsockopt(socket_fd_, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
}

/**
 * connect to server basing on socket_fd_ and socket_ variables
 */
void Client::connectToServer() {
    if (connect(socket_fd_, socket_->ai_addr, socket_->ai_addrlen) == -1)
        throw std::runtime_error("Could not connect to server\n");

    freeaddrinfo(socket_);
}

/**
 * Send given data until a response from server is read.
 * @param data pointer to char array where response will be saved.
 * @param message char array to be sent
 */
void Client::sendImportantPacket(char *data, std::string message) {
    for(int i = 0;; ++i) {
        const char *packet = message.c_str();
        send(packet, message.length());
        usleep(100);
        try {
            receiveFromServer(data, packet_size_);
            break;
        }
        catch (std::runtime_error& e) {
            if (i == 10 )
                throw e;
            std::cerr << e.what() << "\t Sending "<< message <<" again \n";
        }
    }
}


