#ifndef DPDK_SERVER_CLIENT_H
#define DPDK_SERVER_CLIENT_H

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <chrono>
#include <thread>

class Client {
private:
    static const int timeout_ = 500000;
    const std::string SERVER_IP_; //"127.0.0.1"; //should be passed from console
    int socket_fd_;
    addrinfo *socket_;

    static const size_t BUFFER_SIZE_ = 2048;
    char buffer_[BUFFER_SIZE_];
    static const int flags_ = 0;

    size_t packet_size_; // size of a packet to be sent
public:

    /**
     * Gets info about server inits socket and timeout and connects to the server.
     * @param packet_size size in bytes of packets that will be sent
     * @param addressIP ip address of a server client will connect to
     */
    explicit Client(size_t packet_size, std::string addressIP);

    /**
 * sends an array of chars over a socket. Socket must be initialized earlier.
 * @param data char array to send
 */
    void send(char *data, int size);

/**
 * sends an array of chars over a socket. Socket must be initialized earlier.
 * @param data char array to send
 */
    void send(const char *data, int size);

    /**
     * Start a main client activity. Sends HELLO message, a set of packets in given intervals, done message
     * and clear message, loop of sending packets.
     * @param packetCount number of packets to be sent
     * @param intervalBetweenPackets time in micro sec between sending packets
     * @param timeBeforeDone time after sending packets and before asking about number of received packets.
     */
    void startSending(int packetCount, int intervalBetweenPackets, int timeBeforeDone);

    void startReceiving(int packetCount, int intervalBetweenPackets, int timeBeforeDone);

/**
 * create socket connected to new given port.
 * @param port_number
 */
    void connectToNewPort(const char* portNumber);

    /**
 * Receive bytes from a socket
 * @param data pointer to char array where received data will be saved.
 * @param size size of a data to read.
 */
    void receiveFromServer(char *data, size_t size);

    /**
 * Save server address info in class variable socket_
 * @param port_number
 */
    void getServerInfo(const char *port_number);

    /**
 * Initialize socketFd basing on socket_ variable.
 */
    void initSocketFd();
/**
 * set socket receive timeout to class variable _timeout
 */
    void initTimeout();

    /**
     * Set receive timeout
     * @param seconds amount of seconds
     * @param microSeconds amount of microseconds
     */
    void setTimeout(time_t seconds, int microSeconds);
/**
 * connect to server basing on socket_fd_ and socket_ variables
 */
    void connectToServer();
/**
 * Send given data until a response from server is read.
 * @param data pointer to char array where response will be saved.
 * @param message char array to be sent
 */
    void sendImportantPacket(char *data, std::string message);
};


#endif //DPDK_SERVER_CLIENT_H
