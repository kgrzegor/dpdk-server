#ifndef DPDK_SERVER_SERVER_H
#define DPDK_SERVER_SERVER_H

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdexcept>
#include <arpa/inet.h>
#include <errno.h>
#include <unordered_map>
#include <iostream>
#include <functional>
#include <unistd.h>


class Server {
private:

    static const int timeout_ = 500; //TODO from console?;
    addrinfo *socket_;

    /**
     * Process "HELLO" request, which in the single thread server variant means
     * creating a new packet counter (if it doesn't already exist for the given peer),
     * and sending the server port number to the peer (this is for compatibility reasons,
     * to make it possible to use both, single- and multi-threaded server by the same
     * client program).
     * @param peer_addr sockaddr structure that contains the peer address
     * @param peer_addr_len size of the peer_addr object
     */
    virtual void processHELLO(sockaddr peer_addr, socklen_t peer_addr_len);

    /**
     * Process "DONE" request. This request it send by a client that indicates, it is
     * done with sending packets. The server sends the received packet count to
     * the client.
     * @param peer_addr sockaddr structure that contains the peer address
     * @param peer_addr_len size of the peer_addr object
     */
    void processDONE(sockaddr peer_addr, socklen_t peer_addr_len);

    /**
     * After the client finished sending data packets, and informed the server using
     * the "DONE" request, it can request data packet counter reset, on the server
     * side using the "CLEAR" request.
     * @param peer_addr sockaddr structure that contains the peer address
     * @param peer_addr_len size of the peer_addr object
     */
    void processCLEAR(sockaddr peer_addr, socklen_t peer_addr_len);

    /**
     * Increment the data packet counter for a given peer.
     * @param peer_addr sockaddr structure that contains the peer address
     * @param peer_addr_len size of the peer_addr object
     */
    void processData(sockaddr peer_addr, socklen_t peer_addr_len);

    /**
     * "TELLME" request is invalid in case of the single-threaded server variant.
     * end connection by sending "DONE" message to client.
     * @param peer_addr sockaddr structure that contains the peer address
     * @param peer_addr_len size of the peer_addr object
     * @param request comma-separated parameters for TELLME request, which are discarded
     * in single-threaded server variant.
     */
    virtual void processTELLME(sockaddr peer_addr, socklen_t peer_addr_len, std::string request);

    /**
     * This function initializes the 'process_' map, with (annonymous) functions,
     * which call appropriate 'processX' methods.
     */
    void initProcess();


protected:
    static const int flags_ = 0;
    int socket_fd_;
    static const size_t BUFFER_SIZE_ = 2048;
    constexpr static char SERVER_PORT_[6] = "54000";

    /**
     * Map request numbers (encoded as char) to functions, which call appropriate
     * 'processX' methods.
     */
    std::unordered_map<char, std::function<void(sockaddr, socklen_t, std::string)>> process_;

    /**
     * A map: client_port -> amount of send packets
     */
    std::unordered_map<int, int> clients;

    /**
     * Initialize socket, throw runtime error, on initialization failure.
     * @param port the port to which the socket will be attached
     * @param socket the socket address info to be filled
     */
    void initSocket(const char *port, addrinfo **socket);

    /**
     * A parameter-less wrapper for initSocketFd(int &socket_fd, addrinfo *socket),
     * which passes socket_fd_ and socket_ class fields as arguments.
     */
    void initSocketFd();

    /**
     * Initialize create socket descriptor, based on parameters stored in socket. Throw
     * runtime error when socket initialization fails.
     * @param socket_fd reference to the variable to be initialized with a valid socket descriptor
     * @param socket pointer to addrinfo structure to be used for socket_fd initialization
     */
    void initSocketFd(int &socket_fd, addrinfo *socket);

    /**
     * A parameter-less wrapper for bindSocket(int &socket_fd, addrinfo *socket_info),
     * which passes socket_fd_ and socket_ class fields as arguments
     */
    void bindSocket();

    /**
     * Bind socket with descriptor stored in socket_fd to given address, stored in
     * socket info. This allows to replace "sendto" calls with "send" and "write"
     * @param socket_fd reference to socket descriptor of the socket to be bound to address
     * stored in socket_info
     * @param socket_info address to be assigned to socket referred by socket_fd
     */
    void bindSocket(int &socket_fd, addrinfo *socket_info);

    /**
     * Set timeout for recv calls, on socket referred to by socket_fd, to amount
     * of micro-seconds, stored in classfield 'timeout_'
     * @param socket_fd reference to socket descriptor of the socket, which will
     * receive timeout option
     */
    void initTimeout(int &socket_fd);

private:
    char buffer_[BUFFER_SIZE_];

public:
    constexpr static addrinfo HINTS = {AI_PASSIVE, AF_INET, SOCK_DGRAM, IPPROTO_UDP, flags_, nullptr, nullptr, nullptr};

    /**
     * Default constructor which calls "initX" methods.
     */
    Server();

    /**
     * A parameter-less wrapper for receive(int socket_fd, char *buffer),
     * which passes socket_fd_ and buffer_ class fields as arguments
     */
    void receive();

    /**
     * Receive a single message from client. When receive fails, throw runtime error.
     * @param socket_fd the socket descriptor of the socket being read from.
     * @param buffer the buffer to be filled with the received message
     */
    void receive(int socket_fd, char *buffer);

    /**
     * Get IPv4 address the server is listening on.
     * @return IPv4 address encoded as std::string
     */
    std::string getIP();

    /**
     * Get port the server is listening on.
     * @return port number encoded as std::string
     */
    static std::string getPort();

    /**
     * Resolves a port of a client.
     * @param peer_addr address of a client to resolve
     * @param peer_addr_len length of an address structure
     * @return port numer of a client connected
     */
    int getHostInfo(sockaddr peer_addr, socklen_t peer_addr_len);

};


#endif //DPDK_SERVER_SERVER_H
