#include <mutex>
#include <thread>
#include "server.h"

Server::Server() : buffer_{} {
    initProcess();
    initSocket(SERVER_PORT_, &socket_);
    initSocketFd();
    bindSocket();
    initTimeout(socket_fd_);
}

void Server::receive() {
    receive(socket_fd_, buffer_);
}

void Server::receive(int socket_fd, char *buffer) {
    sockaddr peer_addr = {};
    socklen_t peer_addr_len = sizeof(sockaddr);

    ssize_t bytes_read = recvfrom(socket_fd, buffer, BUFFER_SIZE_, flags_, &peer_addr, &peer_addr_len);

    if (bytes_read == -1)
        throw std::runtime_error("Failed to receive request -- timeout\n");
//<debug>
    else if (bytes_read < BUFFER_SIZE_)
        buffer[bytes_read] = '\0';
    else
        buffer[BUFFER_SIZE_-1] = '\0';
//</debug>

    process_[buffer[0]](peer_addr, peer_addr_len, std::string(buffer + 2, buffer + BUFFER_SIZE_));

    /*std::cout << "Thread " << std::this_thread::get_id() << " received " << bytes_read << " bytes from "
              << getHostInfo(peer_addr, peer_addr_len) << "\nData: " << buffer << "\n";*/
}

void Server::initProcess() {
    process_.insert(std::make_pair('0', [this](sockaddr p, socklen_t l, std::string r) { this->processTELLME(p, l, r);}));
    process_.insert(std::make_pair('1', [this](sockaddr p, socklen_t l, std::string r) { this->processHELLO(p, l);}));
    process_.insert(std::make_pair('2', [this](sockaddr p, socklen_t l, std::string r) { this->processData(p, l);}));
    process_.insert(std::make_pair('3', [this](sockaddr p, socklen_t l, std::string r) { this->processDONE(p, l);}));
    process_.insert(std::make_pair('4', [this](sockaddr p, socklen_t l, std::string r) { this->processCLEAR(p, l);}));
}

void Server::initSocket(const char *port, addrinfo** socket) {
    int s = getaddrinfo(nullptr, port, &Server::HINTS, socket);
    if (s != 0) {
        throw std::runtime_error("Can't init socket struct\n");
    }
}
void Server::initSocketFd(){
    initSocketFd(socket_fd_, socket_);
}
void Server::initSocketFd(int& socket_fd, addrinfo* socket_info) {
    socket_fd = socket(socket_info->ai_family, socket_info->ai_socktype, socket_info->ai_protocol);
    if (socket_fd == 0)
        throw std::runtime_error("Can't create a socket\n"); //TODO: other exception class
}
void Server::bindSocket(){
    bindSocket(socket_fd_, socket_);
}
void Server::bindSocket(int& socket_fd, addrinfo* socket_info) {
    if (bind(socket_fd, socket_info->ai_addr, socket_info->ai_addrlen) == -1)
        throw std::runtime_error("Can't bind socket stream\n" + std::string(strerror(errno)));
}

void Server::initTimeout(int& socket_fd) {
    timeval read_timeout = {};
    read_timeout.tv_sec = timeout_;
    read_timeout.tv_usec = 0;
    setsockopt(socket_fd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
}

void Server::processHELLO(sockaddr peer_addr, socklen_t peer_addr_len) {
    size_t len = sizeof(SERVER_PORT_)/sizeof(SERVER_PORT_[0]);
    int port_number = getHostInfo(peer_addr, peer_addr_len);

    if (clients.find(port_number) == clients.end())
        clients.insert(std::make_pair(port_number, 0));

    sendto(socket_fd_, SERVER_PORT_, len, 0, &peer_addr, peer_addr_len);
}

void Server::processData(sockaddr peer_addr, socklen_t peer_addr_len) {
    int port_number = getHostInfo(peer_addr, peer_addr_len);
    clients[port_number] += 1;
}


void Server::processDONE(sockaddr peer_addr, socklen_t peer_addr_len) {
    char data[20];
    int port_number = getHostInfo(peer_addr, peer_addr_len);
    size_t len = sizeof(data)/sizeof(data[0]);
    sprintf(data, "%d", clients[port_number]);
    sendto(socket_fd_, data, len, 0, &peer_addr, peer_addr_len);
}

void Server::processCLEAR(sockaddr peer_addr, socklen_t peer_addr_len) {
    int port_number = getHostInfo(peer_addr, peer_addr_len);
    clients[port_number] = 0;
    sendto(socket_fd_, "Ok", strlen("Ok"), 0, &peer_addr, peer_addr_len);
}

void Server::processTELLME(sockaddr peer_addr, socklen_t peer_addr_len, std::string request)
{
    const char *data = "3 DONE";
    size_t len = strlen(data);
    sendto(socket_fd_, data, len, 0, &peer_addr, peer_addr_len); //sending w/o '\n' should be ok
}

std::string Server::getIP() {
    char str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(((sockaddr_in*)(socket_->ai_addr))->sin_addr), str, INET_ADDRSTRLEN);
    return std::string(str);
}

std::string Server::getPort() {
    return std::string(SERVER_PORT_);
}

int Server::getHostInfo(sockaddr peer_addr, socklen_t peer_addr_len) {
    char host[NI_MAXHOST], port[NI_MAXSERV];

    int s = getnameinfo(&peer_addr, peer_addr_len, host, NI_MAXHOST,
                        port, NI_MAXSERV, NI_NUMERICSERV);

    if (s != 0)
        throw std::runtime_error("Can't get client name\n");

    int host_port_number = static_cast<int>(strtol(port, nullptr, 10));

    return host_port_number;
}
