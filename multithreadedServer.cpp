//
// Created by kgrzegor on 24.05.19.
//

#include "multithreadedServer.h"


MultithreadedServer::MultithreadedServer():Server(),N_PROC_(get_nprocs()),next_port_(0) {
}

void MultithreadedServer::initThreads() {
    size_t len = sizeof(SERVER_PORT_)/sizeof(SERVER_PORT_[0]);
    char new_port[len];
    for (int threadID = 0; threadID < N_PROC_; ++threadID)
    {
        buffers_.push_back(new char[BUFFER_SIZE_]);

        sockets_.push_back(nullptr);
        sprintf(new_port,"%ld", strtol(SERVER_PORT_, nullptr, 10) + threadID + 1);

        initSocket(new_port, &sockets_[threadID]);

        socket_fds_.push_back(0);
        initSocketFd(socket_fds_[threadID], sockets_[threadID]);
        bindSocket(socket_fds_[threadID], sockets_[threadID]);
       // initTimeout(socket_fds_[threadID]);
    }
}

MultithreadedServer::~MultithreadedServer() {
    for(auto& thread : threads_)
        thread.join();
}

void MultithreadedServer::processHELLO(sockaddr peer_addr, socklen_t peer_addr_len) {
    std::lock_guard<std::mutex> guard(new_client_);
    size_t len = sizeof(SERVER_PORT_)/sizeof(SERVER_PORT_[0]);
    int port_number = getHostInfo(peer_addr, peer_addr_len);
    std::string new_port = std::to_string(strtol(SERVER_PORT_, nullptr, 10) + next_port_ + 1);
    next_port_ = (next_port_ + 1) % 1000;
    addrinfo* socket = nullptr;
    while(socket == nullptr){
        try {
            initSocket(new_port.c_str(), &socket);
        } catch (std::runtime_error& e) {
            next_port_ = (next_port_ + 1) % 1000;
            new_port = std::to_string(strtol(SERVER_PORT_, nullptr, 10) + next_port_ + 1);
        }
    }
    std::cout << "Server redirect client to: " << new_port << "\n";

    if (clients.find(port_number) == clients.end()){
        clients.insert(std::make_pair(port_number, 0));
    }

    threads_.emplace_back(std::thread(&MultithreadedServer::receivePackets, this, socket));

    usleep(1000); // this timeout is to make sure that thread is receiving

    sendto(socket_fd_, new_port.c_str(), len, 0, &peer_addr, peer_addr_len);
}

//TODO: redirect work to different thread
void MultithreadedServer::processTELLME(sockaddr peer_addr, socklen_t peer_addr_len, std::string request)
{
    sendto(socket_fd_, "5 OK", strlen("5 OK"), 0, &peer_addr, peer_addr_len); //sending w/o '\n' should be ok

    std::stringstream ss(request);

    int packetCount, packetSize, intervalBetweenPackets, timeBeforeDone;
    char a;

    ss >> packetCount >> a >> packetSize >> a >> intervalBetweenPackets >> a >> timeBeforeDone;

    std::cout << "Received request to send: " << packetCount << " packets, of size: " << packetSize
              << ", with: " << intervalBetweenPackets << " interval, and: " << timeBeforeDone << " time before done\n";

    threads_.emplace_back(std::thread(&MultithreadedServer::sendPackets, this, packetCount, packetSize,
                          intervalBetweenPackets, timeBeforeDone, peer_addr, peer_addr_len));
}

void MultithreadedServer::sendPackets(int packetCount, int packetSize, int intervalBetweenPackets, int timeBeforeDone,
                                      sockaddr peer_addr, socklen_t peer_addr_len)
{
    char *data = new char[packetSize];

    for (int i = 0; i < packetCount; ++i) {
        sprintf(data, "2 PACKET_ID %d", i);
        std::chrono::system_clock::time_point timeLimit = std::chrono::system_clock::now() +
                                                          std::chrono::microseconds(intervalBetweenPackets);

        sendto(socket_fd_, data, packetSize, 0, &peer_addr, peer_addr_len);

        std::this_thread::sleep_until(timeLimit);
    }

    std::this_thread::sleep_for(std::chrono::microseconds(timeBeforeDone));
    sendto(socket_fd_, "3 DONE", strlen("3 DONE"), 0, &peer_addr, peer_addr_len); //sending w/o '\n' should be ok

    delete [] data;

}

void MultithreadedServer::startThreads() {
    for (int threadID = 0; threadID < N_PROC_; ++threadID)
        threads_.emplace_back(std::thread([this, threadID]{
            for(;;)
                receive(socket_fds_[threadID], buffers_[threadID]);
        }));
}

void MultithreadedServer::startThread(int socket_fd, char* buffer) {
    threads_.emplace_back(std::thread([this, socket_fd, buffer]{
        for(;;)
            receive(socket_fd, buffer);
    }));
}

void MultithreadedServer::receivePackets(addrinfo* socket) {

    //size_t len = sizeof(SERVER_PORT_)/sizeof(SERVER_PORT_[0]);
    //char new_port[len];

    // addrinfo* socket = nullptr;

    // sprintf(new_port,"%ld", strtol(SERVER_PORT_, nullptr, 10) + threadID + 1);

    // initSocket(new_port, &socket);

    int local_socket_fd = 0;

    initSocketFd(local_socket_fd, socket);
    bindSocket(local_socket_fd, socket);


    char buffer[BUFFER_SIZE_];
    int received = 0;
    bool finished = false;
    while(!finished)
    {
        sockaddr peer_addr = {};
        socklen_t peer_addr_len = sizeof(sockaddr);

        ssize_t bytes_read = recvfrom(local_socket_fd, buffer, BUFFER_SIZE_, flags_, &peer_addr, &peer_addr_len);

        if (bytes_read == -1)
            throw std::runtime_error("Failed to receive request -- timeout\n");
//<debug>
        else if (bytes_read < BUFFER_SIZE_)
            buffer[bytes_read] = '\0';
        else
            buffer[BUFFER_SIZE_-1] = '\0';
//</debug>

        switch(buffer[0]){
            case '2':
                ++received;
                break;
            case '3': {
                char data[20];
                size_t len = sizeof(data) / sizeof(data[0]);
                sprintf(data, "%d", received);
                sendto(local_socket_fd, data, len, 0, &peer_addr, peer_addr_len);
                break;
            }
            case '4':
                finished = true;
                sendto(local_socket_fd, "Ok", strlen("Ok"), 0, &peer_addr, peer_addr_len);
                break;
            default:
                break;
        }



        // process_[buffer[0]](peer_addr, peer_addr_len, std::string(buffer + 2, buffer + BUFFER_SIZE_));

        // std::cout << "Thread " << std::this_thread::get_id() << " received " << bytes_read << " bytes from "
        //          << getHostInfo(peer_addr, peer_addr_len) << "\nData: " << buffer << "\n";
    }
}

void MultithreadedServer::sendImportantPacket(char *data, std::string message) {
//    for(int i = 0;; ++i) {
//        const char *packet = message.c_str();
//        send(packet, message.length());
//        usleep(100);
//        try {
//            receiveFromServer(data, packet_size_);
//            break;
//        }
//        catch (std::runtime_error& e) {
//            if (i == 10 )
//                throw e;
//            std::cerr << e.what() << "\t Sending "<< message <<" again \n";
//        }
//    }
}
